import React from 'react';

const Cheeseburger = props => {
    if (props.cheeseburger.amount) {
        return (
            <div className="order">Cheeseburger x {props.cheeseburger.amount} {props.cheeseburger.price} KGS<i onClick={props.removeOrder} className="fa fa-times" aria-hidden="true"/></div>
        )
    } else {
        return (
            null
        )
    }
};

export default Cheeseburger;