import React from 'react';

const Coffee = props => {
    if (props.coffee.amount) {
        return (
            <div className="order">Coffee x {props.coffee.amount} {props.coffee.price} KGS<i onClick={props.removeOrder} className="fa fa-times" aria-hidden="true"/></div>
        )
    } else {
        return (
            null
        )
    }
};

export default Coffee;