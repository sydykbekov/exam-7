import React from 'react';

const Hamburger = props => {
    if (props.hamburger.amount) {
        return (
             <div className="order">Hamburger x {props.hamburger.amount} {props.hamburger.price} KGS<i onClick={props.removeOrder} className="fa fa-times" aria-hidden="true"/></div>
        )
    } else {
        return (
            null
        )
    }
};

export default Hamburger;