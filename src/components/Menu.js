import React from 'react';

const Menu = props => {
   return (
        <div className="food-menu">
            <h3>Menu</h3>
            <div className="meal food" onClick={props.addHamburger}>
                <i className="fa fa-cutlery" aria-hidden="true"/>
                <span>Hamburger</span><br/>
                <span>Price: 80 KGS</span>
            </div>
            <div className="meal food" onClick={props.addCheeseburger}>
                <i className="fa fa-cutlery" aria-hidden="true"/>
                <span>Cheeseburger</span><br/>
                <span>Price: 90 KGS</span>
            </div>
            <div className="meal food" onClick={props.addFries}>
                <i className="fa fa-cutlery" aria-hidden="true"/>
                <span>Fries</span><br/>
                <span>Price: 45 KGS</span>
            </div>
            <div className="meal drink" onClick={props.addCoffee}>
                <i className="fa fa-coffee" aria-hidden="true"/>
                <span>Coffee</span><br/>
                <span>Price: 70 KGS</span>
            </div>
            <div className="meal drink" onClick={props.addTea}>
                <i className="fa fa-coffee" aria-hidden="true"/>
                <span>Tea</span><br/>
                <span>Price: 50 KGS</span>
            </div>
            <div className="meal drink" onClick={props.addCola}>
                <i className="fa fa-coffee" aria-hidden="true"/>
                <span>Cola</span><br/>
                <span>Price: 40 KGS</span>
            </div>
        </div>
   )
};

export default Menu;