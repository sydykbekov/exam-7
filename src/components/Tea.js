import React from 'react';

const Tea = props => {
    if (props.tea.amount) {
        return (
            <div className="order">Tea x {props.tea.amount} {props.tea.price} KGS<i onClick={props.removeOrder} className="fa fa-times" aria-hidden="true"/></div>
        )
    } else {
        return (
            null
        )
    }
};

export default Tea;