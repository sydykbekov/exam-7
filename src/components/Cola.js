import React from 'react';

const Cola = props => {
    if (props.cola.amount) {
        return (
            <div className="order">Cola x {props.cola.amount} {props.cola.price} KGS<i onClick={props.removeOrder} className="fa fa-times" aria-hidden="true"/></div>
        )
    } else {
        return (
            null
        )
    }
};

export default Cola;