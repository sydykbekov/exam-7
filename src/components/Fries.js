import React from 'react';

const Fries = props => {
    if (props.fries.amount) {
        return (
            <div className="order">Fries x {props.fries.amount} {props.fries.price} KGS<i onClick={props.removeOrder} className="fa fa-times" aria-hidden="true"/></div>
        )
    } else {
        return (
            null
        )
    }
};

export default Fries;