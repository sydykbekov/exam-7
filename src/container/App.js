import React, {Component} from 'react';
import './App.css';
import Menu from '../components/Menu';
import Hamburger from '../components/Hamburger';
import Cheeseburger from '../components/Cheeseburger';
import Fries from '../components/Fries';
import Coffee from '../components/Coffee';
import Tea from '../components/Tea';
import Cola from '../components/Cola';


class App extends Component {
    state = {
        hamburger: {amount: null, price: 0},
        cheeseburger: {amount: null, price: 0},
        fries: {amount: null, price: 0},
        coffee: {amount: null, price: 0},
        tea: {amount: null, price: 0},
        cola: {amount: null, price: 0},
        total: null
    };

    addHamburger = () => {
        let state = {...this.state};
        state.hamburger.amount++;
        state.hamburger.price += 80;
        state.total += 80;
        this.setState(state);
    };

    removeHamburger = () => {
        let state = {...this.state};
        state.total -= state.hamburger.price;
        state.hamburger.amount = null;
        state.hamburger.price = 0;
        this.setState(state);
    };

    addCheeseburger= () => {
        let state = {...this.state};
        state.cheeseburger.amount++;
        state.cheeseburger.price += 90;
        state.total += 90;
        this.setState(state);
    };

    removeCheeseburger = () => {
        let state = {...this.state};
        state.total -= state.cheeseburger.price;
        state.cheeseburger.amount = null;
        state.cheeseburger.price = 0;
        this.setState(state);
    };

    addFries = () => {
        let state = {...this.state};
        state.fries.amount++;
        state.fries.price += 45;
        state.total += 45;
        this.setState(state);
    };

    removeFries = () => {
        let state = {...this.state};
        state.total -= state.fries.price;
        state.fries.amount = null;
        state.fries.price = 0;
        this.setState(state);
    };

    addCoffee = () => {
        let state = {...this.state};
        state.coffee.amount++;
        state.coffee.price += 60;
        state.total += 60;
        this.setState(state);
    };

    removeCoffee = () => {
        let state = {...this.state};
        state.total -= state.coffee.price;
        state.coffee.amount = null;
        state.coffee.price = 0;
        this.setState(state);
    };

    addTea = () => {
        let state = {...this.state};
        state.tea.amount++;
        state.tea.price += 50;
        state.total += 50;
        this.setState(state);
    };

    removeTea = () => {
        let state = {...this.state};
        state.total -= state.tea.price;
        state.tea.amount = null;
        state.tea.price = 0;
        this.setState(state);
    };

    addCola = () => {
        let state = {...this.state};
        state.cola.amount++;
        state.cola.price += 40;
        state.total += 40;
        this.setState(state);
    };

    removeCola = () => {
        let state = {...this.state};
        state.total -= state.cola.price;
        state.cola.amount = null;
        state.cola.price = 0;
        this.setState(state);
    };

    render() {
        return (
            <div className="App">
                <div className="burgers">
                    <h3 className="title">Ordered meals</h3>
                    <Hamburger hamburger={this.state.hamburger} removeOrder={this.removeHamburger}/>
                    <Cheeseburger cheeseburger={this.state.cheeseburger} removeOrder={this.removeCheeseburger}/>
                    <Fries fries={this.state.fries} removeOrder={this.removeFries}/>
                </div>
                <div className="drinks">
                    <h3 className="title">Ordered drinks</h3>
                    <Coffee coffee={this.state.coffee} removeOrder={this.removeCoffee}/>
                    <Tea tea={this.state.tea} removeOrder={this.removeTea}/>
                    <Cola cola={this.state.cola} removeOrder={this.removeCola}/>
                </div>
                <div className="totalPrice">{this.state.total ? `Total price: ${this.state.total} KGS` : null}</div>
                <Menu addHamburger={this.addHamburger} addCheeseburger={this.addCheeseburger} addFries={this.addFries} addCoffee={this.addCoffee} addTea={this.addTea} addCola={this.addCola} />
            </div>
        );
    }
}

export default App;
